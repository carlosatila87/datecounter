﻿import React, { Component } from 'react';
import WorkDaysCounterComponent from './WorkDaysCounter.component';
import axios from 'axios';
import moment from 'moment';

class WorkDaysCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstDate: null,
            secondDate: null,
            holidayDate: null,
            holidays:[],
            result: null
        }
    }

    handleCalculateAction = () => {
        const { firstDate, secondDate, holidays } = this.state;
        const formattedFirstDate = moment(firstDate).format('YYYYMMDD');
        const formattedSecondDate = moment(secondDate).format('YYYYMMDD');
        const payload = {
            firstDate: formattedFirstDate,
            secondDate: formattedSecondDate,
            holidays: holidays
        };
        const options = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        axios.post(`api/DataHandler/CountBusinessDays`, payload, options).then(
            res => {
                const { count } = res.data;
                this.setState({
                    result: count.toString()
                });
            }
        ).catch(
            err => { console.log(err.response); }
        );
    }

    handleOnChange = (e, field) => {
        this.setState({
            [field]: e
        });
    }

    handleAddHoliday = (holiday) => {
        const formattedHoliday = moment(holiday).format('YYYYMMDD');
        let { holidays } = this.state;
        holidays.push(formattedHoliday);
        this.setState({
            holidays,
            holidayDate: null
        });
    }

    render() {
        const { firstDate, secondDate, holidayDate, result, holidays } = this.state;
        return (
            <WorkDaysCounterComponent
                firstDate={firstDate}
                secondDate={secondDate}
                holidayDate={holidayDate}
                holidays={holidays}
                result={result}
                calculateAction={this.handleCalculateAction}
                onChange={this.handleOnChange}
                addHoliday={this.handleAddHoliday}
            />
        );
    }
}

export default WorkDaysCounter;