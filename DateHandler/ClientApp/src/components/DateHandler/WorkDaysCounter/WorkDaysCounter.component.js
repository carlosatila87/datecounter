﻿import React, { Fragment } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

const WorkDaysCounterComponent = ({ firstDate, secondDate, holidayDate, holidays, onChange, calculateAction, addHoliday, result }) => (
    <div>
        <h5>Count Business Days</h5>
        <div className="row align-items-end">
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="First Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={firstDate}
                    onChange={e => onChange(e, 'firstDate')}
                />
            </div>
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="Second Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={secondDate}
                    onChange={e => onChange(e, 'secondDate')}
                />
            </div>
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="Holiday Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={holidayDate}
                    onChange={e => onChange(e, 'holidayDate')}
                />
            </div>
            <div className="col-md-2">
                <button type="button"
                    disabled={!holidayDate}
                    className="btn btn-secondary"
                    onClick={() => addHoliday(holidayDate)}
                >Add holiday</button>
            </div>
            <div className="col col-md-2">
                <button
                    disabled={!(firstDate && secondDate)}
                    type="button"
                    className="btn btn-primary"
                    onClick={calculateAction}
                >
                    Calculate
                </button>
            </div>
            <div className="col-md-2">
                {result ? <p><strong>Result: {result} day(s)</strong></p> : null}
            </div>
        </div>
        <div className="row align-items-end">
            <div className="col col-md-2">
                {holidays.length > 0 ?
                    <Fragment>
                        <br />
                        <table className="table">
                        <thead>
                            <tr>
                                <th>Holiday Dates</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            {
                                holidays.map(holidayItem =>
                                    <tr key={holidayItem}>
                                        <td>{moment(holidayItem).format('DD/MM/YYYY')}</td>
                                        </tr>
                                )
                            }
                        </tbody>
                        </table>
                        </Fragment>
                    : null}
            </div>
        </div>
    </div>
);

export default WorkDaysCounterComponent;