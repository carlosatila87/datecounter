﻿import React from 'react';
import DatePicker from 'react-datepicker';

const WeekDaysCounterComponent = ({ firstDate, secondDate, onChange, calculateAction, result }) => (
    <div>
        <h5>Count Week Days</h5>
        <div className="row align-items-end">
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="First Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={firstDate}
                    onChange={e => onChange(e, 'firstDate')}
                />
            </div>
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="Second Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={secondDate}
                    onChange={e => onChange(e, 'secondDate')}
                />
            </div>
            <div className="col col-md-2">
                <button
                    disabled={!(firstDate && secondDate)}
                    type="button"
                    className="btn btn-primary"
                    onClick={calculateAction}
                >
                    Calculate
                </button>
            </div>
            <div className="col col-md-2">
                {result ? <p><strong>Result: {result} day(s)</strong></p> : null}
            </div>
        </div>
    </div>
);

export default WeekDaysCounterComponent;