﻿import React, { Component } from 'react';
import WeekDaysCounterComponent from './WeekDaysCounter.component';
import axios from 'axios';
import moment from 'moment';

class WeekDaysCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstDate: null,
            secondDate: null,
            result: null
        }
    }

    handleCalculateAction = () => {
        const { firstDate, secondDate } = this.state;
        const formattedFirstDate = moment(firstDate).format('YYYYMMDD');
        const formattedSecondDate = moment(secondDate).format('YYYYMMDD');
        axios.get(`api/DataHandler/CountWeekDays?firstDate=${formattedFirstDate}&secondDate=${formattedSecondDate}`).then(
            res => {
                const { count } = res.data;
                this.setState({
                    result: count.toString()
                });
            }
        ).catch(
            err => { console.log(err.response); }
        );
    }

    handleOnChange = (e, field) => {
        this.setState({
            [field]: e
        });
    }


    render() {
        const { firstDate, secondDate, result } = this.state;
        return (
            <WeekDaysCounterComponent
                firstDate={firstDate}
                secondDate={secondDate}
                result={result}
                calculateAction={this.handleCalculateAction}
                onChange={this.handleOnChange}
            />
        );
    }
}

export default WeekDaysCounter;