﻿import React, { Fragment } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

const WorkDaysCounterV2Component = ({ firstDate, secondDate, holidayDate, category, ordinality, dayOfWeek, month, holidays, onChange, calculateAction, addHoliday, result, onSelectChange }) => (
    <div>
        <h5>Count Business Days V2</h5>
        <div className="row align-items-end">
            <div className="col col-md-2">
                <DatePicker
                    placeholderText="First Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={firstDate}
                    onChange={e => onChange(e, 'firstDate')}
                />
            </div>
            <div className="col col-md-2">
                    <DatePicker
                        placeholderText="Second Date"
                    dateFormat="dd/MM/yyyy"
                    className="form-control"
                    selected={secondDate}
                    onChange={e => onChange(e, 'secondDate')}
                />
            </div>
            <div className="col col-md-2">
                <button
                    disabled={!(firstDate && secondDate)}
                    type="button"
                    className="btn btn-primary"
                    onClick={calculateAction}
                >
                    Calculate
                </button>
            </div>
            <div className="col-md-2">
                {result ? <p><strong>Result: {result} day(s)</strong></p> : null}
            </div>
        </div>
        <br/>
            <div className="row align-items-end">
            <div className="col col-md-2">
                <select name="category" onChange={onSelectChange} value={category} className="form-control">
                    <option value="">Category</option>
                    <option value="0">Same Day</option>
                    <option value="1">Except on Weekends</option>
                    <option value="2">Ocurrence</option>
                </select>
            </div>
            {category === "0" || category === "1" ?
                <div className="col col-md-2">
                    <DatePicker
                        placeholderText="Holiday Date"
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                        selected={holidayDate}
                        onChange={e => onChange(e, 'holidayDate')}
                    />
                </div>
                : null}
            {category === "2" ?
                <Fragment>
                <div className="col col-md-2">
                    <select name="ordinality" onChange={onSelectChange} value={ordinality} className="form-control">
                        <option value="">Ordinality</option>
                        <option value="1">First</option>
                        <option value="2">Second</option>
                        <option value="3">Third</option>
                        <option value="4">Fourth</option>
                        <option value="5">Last</option>
                    </select>
                </div>
                <div className="col col-md-2">
                    <select name="dayOfWeek" onChange={onSelectChange} value={dayOfWeek} className="form-control">
                        <option value="">Day of Week</option>
                        <option value="0">Sunday</option>
                        <option value="1">Monday</option>
                        <option value="2">Tuesday</option>
                        <option value="3">Wednesday</option>
                        <option value="4">Thursday</option>
                        <option value="5">Friday</option>
                        <option value="6">Saturday</option>
                    </select>
                </div>
                <div className="col col-md-2">
                    <select name="month" onChange={onSelectChange} value={month} className="form-control">
                        <option value="">Month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                    </div>
                </Fragment>
                : null}
            <div className="col-md-2">
                <button type="button"
                    disabled={!(((category === "0" || category === "1") && holidayDate) || (category === "2" && ordinality !== "" && dayOfWeek !== "" && month!==""))}
                    className="btn btn-secondary"
                    onClick={addHoliday}
                >Add holiday</button>
            </div>
        </div>
        <div className="row align-items-end">
            <div className="col col-md-6">
                {holidays.length > 0 ?
                    <Fragment>
                    <br/>
                        <table className="table">
                        <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Holiday Date</th>
                                    <th>Ordinality</th>
                                    <th>Day of Week</th>
                                    <th>Month</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                holidays.map(holidayItem =>
                                    <tr key={holidayItem.Counter}>
                                        <td>{holidayItem.Category === 0 ? "Same Day" : holidayItem.Category === 1 ? "Except on Weekends" : "Ocurrence"} </td>
                                        <td>{holidayItem.Date ? moment(holidayItem.Date).format('DD/MM/YYYY') : null}</td>
                                        <td>{holidayItem.Ordinality === null ? null : holidayItem.Ordinality === 1 ? "First" : holidayItem.Ordinality === 2 ? "Second" : holidayItem.Ordinality === 3 ? "Third" : holidayItem.Ordinality === 4 ? "Fourth" : "Last"} </td>
                                        <td>{holidayItem.DayOfWeek === null ? null : holidayItem.DayOfWeek === 0 ? "Sunday" : holidayItem.DayOfWeek === 1 ? "Monday" : holidayItem.DayOfWeek === 2 ? "Tuesday" : holidayItem.DayOfWeek === 3 ? "Wednesday" : holidayItem.DayOfWeek === 4 ? "Thursday" : holidayItem.DayOfWeek === 5 ? "Friday" : "Saturday"} </td>
                                        <td>{holidayItem.Month ? holidayItem.Month.toString() : null}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                        </table>
                    </Fragment>
                    : null}
            </div>
        </div>
    </div>
);

export default WorkDaysCounterV2Component;