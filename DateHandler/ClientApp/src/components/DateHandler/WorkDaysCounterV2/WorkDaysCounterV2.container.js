﻿import React, { Component } from 'react';
import WorkDaysCounterV2Component from './WorkDaysCounterV2.component';
import axios from 'axios';
import moment from 'moment';

class WorkDaysCounterV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstDate: null,
            secondDate: null,
            category: null,
            holidayDate: null,
            ordinality: null,
            dayOfWeek: null,
            month: null,
            holidays: [],
            result: null,
            counter: 0,
        }
    }

    handleCalculateAction = () => {
        const { firstDate, secondDate, holidays } = this.state;
        const formattedFirstDate = moment(firstDate).format('YYYYMMDD');
        const formattedSecondDate = moment(secondDate).format('YYYYMMDD');
        const payload = {
            firstDate: formattedFirstDate,
            secondDate: formattedSecondDate,
            holidays: holidays
        };
        const options = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        axios.post(`api/DataHandler/CountBusinessDaysV2`, payload, options).then(
            res => {
                const { count } = res.data;
                this.setState({
                    result: count.toString()
                });
            }
        ).catch(
            err => { console.log(err.response); }
        );
    }

    handleOnChange = (e, field) => {
        this.setState({
            [field]: e
        });
    }

    handleAddHoliday = () => {
        const { category, holidayDate, ordinality, dayOfWeek, month, counter } = this.state;
        const formattedHoliday = holidayDate ? moment(holidayDate).format('YYYYMMDD') : null;
        let { holidays } = this.state;
        const newHoliday = {
            Category: category,
            Date: formattedHoliday,
            Ordinality: ordinality,
            DayOfWeek: dayOfWeek,
            Month: month,
            Counter: counter + 1
        };
        holidays.push(newHoliday);
        this.setState({
            holidays,
            category: null,
            holidayDate: null,
            ordinality: null,
            dayOfWeek: null,
            month: null,
            counter: counter + 1
        });
    }

    handleSelectChange = e => {
        const { name, value } = e.target;
        if (name === "category") {
            this.setState({
                [name]: value === "" ? null : parseInt(value, 10),
                holidayDate: null,
                ordinality: null,
                dayOfWeek: null,
                month: null
            });
        }
        else {
            this.setState({
                [name]: value === "" ? null : parseInt(value, 10)
            });
        }
    }

    render() {
        const { firstDate, secondDate, category, holidayDate, ordinality, dayOfWeek, month, result, holidays } = this.state;
        return (
            <WorkDaysCounterV2Component
                firstDate={firstDate}
                secondDate={secondDate}
                category={category !== null ? category.toString() : ""}
                holidayDate={holidayDate}
                ordinality={ordinality ? ordinality.toString() : ""}
                dayOfWeek={dayOfWeek !== null ? dayOfWeek.toString() : ""}
                month={month ? month.toString() : ""}
                holidays={holidays}
                result={result}
                calculateAction={this.handleCalculateAction}
                onChange={this.handleOnChange}
                addHoliday={this.handleAddHoliday}
                onSelectChange={this.handleSelectChange}
            />
        );
    }
}

export default WorkDaysCounterV2;