﻿import React from 'react';
import WeekDaysCounter from './WeekDaysCounter/WeekDaysCounter.container';
import WorkDaysCounter from './WorkDaysCounter/WorkDaysCounter.container';
import WorkDaysCounterV2 from './WorkDaysCounterV2/WorkDaysCounterV2.container';


const DateHandlerComponent = () => (
    <div>
        <WeekDaysCounter />
        <br/>
        <WorkDaysCounter />
        <br/>
        <WorkDaysCounterV2 />
    </div>
);

export default DateHandlerComponent;