import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>Week and Business Days Counter</h1>
        <p>Welcome to my technical test:</p>
        <ul>
          <li>Week Days Counter</li>
          <li>Business Days Counter (By passing Holiday Dates List)</li>
          <li>Business Days Counter (By creating more complex Holiday Rules)</li>
        </ul>
        <p>Thank you for the opportunity.</p>
      </div>
    );
  }
}
