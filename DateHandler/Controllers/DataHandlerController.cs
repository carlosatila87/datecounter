﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DateHandler.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace DateHandler.Controllers
{
    [Route("api/[controller]")]
    public class DataHandlerController : Controller
    {
        [HttpGet("[action]")]
        public IActionResult CountWeekDays([FromQuery] string firstDate, [FromQuery] string secondDate)
        {
            try
            {
                var fDate = DateTime.ParseExact(firstDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sDate = DateTime.ParseExact(secondDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var result = new DaysCounter().WeekdaysBetweenTwoDates(fDate, sDate);
                return Ok(new { count = result });
            }
            catch
            {
                return BadRequest(new { errorMessage = "Invalid date format." }); 
            }
        }

        public class BusinessDayRequestV1
        {
            public string firstDate { get; set; }
            public string secondDate { get; set; }
            public List<string> holidays { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult CountBusinessDays([FromBody] BusinessDayRequestV1 payload)
        {
            try
            {
                var fDate = DateTime.ParseExact(payload.firstDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sDate = DateTime.ParseExact(payload.secondDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var holidays = new List<DateTime>();
                foreach(var holidayStr in payload.holidays)
                {
                    holidays.Add(DateTime.ParseExact(holidayStr, "yyyyMMdd", CultureInfo.InvariantCulture));
                }
                var result = new DaysCounter().BusinessDayBetweenTwoDates(fDate, sDate, holidays);
                return Ok(new { count = result });
            }
            catch
            {
                return BadRequest(new { errorMessage = "Invalid date format." });
            }
        }

        [HttpPost("[action]")]
        public IActionResult CountBusinessDaysV2([FromBody] JObject payload)
        {
            try
            {
                var firstDate = payload["firstDate"].ToString();
                var secondDate = payload["secondDate"].ToString();
                var fDate = DateTime.ParseExact(firstDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var sDate = DateTime.ParseExact(secondDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var holidaysJSON = payload["holidays"];
                var holidays = new List<PublicHoliday>();
                foreach (var holiday in holidaysJSON)
                {
                    var holidayDate = new DateTime();
                    if (!string.IsNullOrEmpty(holiday["Date"].ToString()))
                        holidayDate = DateTime.ParseExact(holiday["Date"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    holidays.Add(new PublicHoliday()
                    {
                        Category = (HolidayCategory)(Convert.ToInt32(holiday["Category"])),
                        Date = holidayDate,
                        Ordinality = holiday["Ordinality"].Type == JTokenType.Null ? HolidayOrdinality.First : (HolidayOrdinality)(Convert.ToInt32(holiday["Ordinality"])),
                        DayOfWeek = holiday["DayOfWeek"].Type == JTokenType.Null? DayOfWeek.Sunday : (DayOfWeek)(Convert.ToInt32(holiday["DayOfWeek"])),
                        Month = holiday["Month"].Type == JTokenType.Null ? 0 : (Convert.ToInt32(holiday["Month"]))
                    }); ;
                }
                var result = new DaysCounter().BusinessDayBetweenTwoDates(fDate, sDate, holidays);
                return Ok(new { count = result });
            }
            catch
            {
                return BadRequest(new { errorMessage = "Invalid date format." });
            }
        }
    }
}