﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DateHandler.Model
{
    public class DaysCounter : IDaysCounter
    {
        private static readonly DayOfWeek[] WEEKEND = { DayOfWeek.Saturday, DayOfWeek.Sunday };

        public int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            if (firstDate == DateTime.MinValue)
                throw new ArgumentNullException("First Date cannot be null");
            if (secondDate == DateTime.MinValue)
                throw new ArgumentNullException("Second Date cannot be null");

            if (secondDate <= firstDate)
              return 0;

            var qtyDays = secondDate.AddDays(-1) - firstDate;
            var result = 0; 
            int weekDays = 0;
            int completeWeekQty = (qtyDays.Days / 7);
            switch (firstDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:        
                    weekDays = (qtyDays.Days % 7) == 0 || (qtyDays.Days % 7) == 1 ? 0 : (qtyDays.Days % 7) - 1;
                    break;
                case DayOfWeek.Sunday:
                    weekDays = (qtyDays.Days % 7) == 0 ? 0 : (qtyDays.Days % 7) == 6 ? 5 : (qtyDays.Days % 7);
                    break;
                case DayOfWeek.Monday:
                    weekDays = (qtyDays.Days % 7) >= 5 ? 4 : (qtyDays.Days % 7);
                    break;
                case DayOfWeek.Tuesday:
                    weekDays = (qtyDays.Days % 7) == 4 || (qtyDays.Days % 7) == 5 ? 3 : (qtyDays.Days % 7) == 6 ? 4 : (qtyDays.Days % 7);
                    break;
                case DayOfWeek.Wednesday:
                    weekDays = (qtyDays.Days % 7) == 3 || (qtyDays.Days % 7) == 4 ? 2 : (qtyDays.Days % 7) >= 5 ? (qtyDays.Days % 7) - 2 : (qtyDays.Days % 7);
                    break;
                case DayOfWeek.Thursday:
                    weekDays = (qtyDays.Days % 7) == 2 || (qtyDays.Days % 7) == 3 ? 1 : (qtyDays.Days % 7) >= 4 ? (qtyDays.Days % 7) - 2 : (qtyDays.Days % 7);
                    break;
                case DayOfWeek.Friday:
                    weekDays = (qtyDays.Days % 7) == 1 || (qtyDays.Days % 7) == 2 ? 0 : (qtyDays.Days % 7) >= 3 ? (qtyDays.Days % 7) - 2 : (qtyDays.Days % 7);
                    break;
                default:
                    break;
            }
            result = (completeWeekQty * 5) + weekDays;
            return result;
        }

        public int BusinessDayBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays)
        {
            if (firstDate == DateTime.MinValue)
                throw new ArgumentNullException("First Date cannot be null");
            if (secondDate == DateTime.MinValue)
                throw new ArgumentNullException("Second Date cannot be null");

            if (secondDate <= firstDate)
                return 0;
            var weekDays = WeekdaysBetweenTwoDates(firstDate, secondDate);
            var rangeHolidays = publicHolidays.Where(holiday => holiday > firstDate && holiday < secondDate).ToList();
            var holidayQty = 0;
            foreach(var holiday in rangeHolidays.Distinct())
            {
                if (!WEEKEND.Contains(holiday.DayOfWeek))
                    holidayQty++;
            }
            var result = weekDays - holidayQty;
            return result;
        }

        public int BusinessDayBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<PublicHoliday> publicHolidays)
        {
            if (firstDate == DateTime.MinValue)
                throw new ArgumentNullException("First Date cannot be null");
            if (secondDate == DateTime.MinValue)
                throw new ArgumentNullException("Second Date cannot be null");

            if (secondDate <= firstDate)
                return 0;
            var holidayDates = new List<DateTime>();
            foreach(var holiday in publicHolidays)
            {
                switch (holiday.Category)
                {
                    case HolidayCategory.SameDay:
                        if (holiday.Date == DateTime.MinValue)
                            throw new ArgumentNullException("Holiday Date cannot be null");
                        holidayDates.Add(holiday.Date);
                        break;
                    case HolidayCategory.ExceptOnWeekends:
                        if (holiday.Date == DateTime.MinValue)
                            throw new ArgumentNullException("Holiday Date cannot be null");
                        if (!WEEKEND.Contains(holiday.Date.DayOfWeek))
                            holidayDates.Add(holiday.Date);
                        else
                            holidayDates.Add(holiday.Date.AddDays(holiday.Date.DayOfWeek == DayOfWeek.Saturday ? 2 : 1));
                        break;
                    case HolidayCategory.ByOcurrence:
                        if (holiday.Month > 12 || holiday.Month <= 0)
                            throw new ArgumentOutOfRangeException("Month value is out of range");
                        if (Convert.ToInt32(holiday.Ordinality) <= 0 || Convert.ToInt32(holiday.Ordinality) > 5)
                            throw new ArgumentOutOfRangeException("Ordinality value is out of range");
                        if (Convert.ToInt32(holiday.DayOfWeek) < 0 || Convert.ToInt32(holiday.DayOfWeek) > 6)
                            throw new ArgumentOutOfRangeException("Day of week value is out of range");
                        var firstYear = firstDate.Year;
                        var secondYear = secondDate.Year;
                        for(var theYear = firstYear; theYear <= secondYear; theYear++)
                        {
                            var firstDayOfMonth = new DateTime(theYear, holiday.Month, 1);
                            var firstWeekDay = new DateTime();
                            if (firstDayOfMonth.DayOfWeek == holiday.DayOfWeek)
                                firstWeekDay = firstDayOfMonth;
                            else
                            {
                                if (holiday.DayOfWeek - firstDayOfMonth.DayOfWeek > 0)
                                    firstWeekDay = firstDayOfMonth.AddDays(holiday.DayOfWeek - firstDayOfMonth.DayOfWeek);
                                else
                                    firstWeekDay = firstDayOfMonth.AddDays(7 + (holiday.DayOfWeek - firstDayOfMonth.DayOfWeek));
                            }
                            if (holiday.Ordinality == HolidayOrdinality.First)
                                holidayDates.Add(firstWeekDay);
                            else if (holiday.Ordinality == HolidayOrdinality.Second)
                                holidayDates.Add(firstWeekDay.AddDays(7));
                            else if (holiday.Ordinality == HolidayOrdinality.Third)
                                holidayDates.Add(firstWeekDay.AddDays(14));
                            else if (holiday.Ordinality == HolidayOrdinality.Fourth)
                                holidayDates.Add(firstWeekDay.AddDays(21));
                            else if (holiday.Ordinality == HolidayOrdinality.Last)
                                holidayDates.Add(firstWeekDay.AddDays(28).Month == holiday.Month ? firstWeekDay.AddDays(28) : firstWeekDay.AddDays(21));
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("Category is out of range");
                }
            }
            return BusinessDayBetweenTwoDates(firstDate, secondDate, holidayDates);
        }
    }
}
