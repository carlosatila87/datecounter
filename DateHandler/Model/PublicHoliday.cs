﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DateHandler.Model
{
    public enum HolidayCategory {
        SameDay = 0,
        ExceptOnWeekends = 1,
        ByOcurrence = 2
    };

    public enum HolidayOrdinality
    {
        First = 1,
        Second = 2,
        Third = 3,
        Fourth = 4,
        Last = 5
    };

    public class PublicHoliday
    {
        public HolidayCategory Category { get; set; }
        public DateTime Date { get; set; }
        public HolidayOrdinality Ordinality { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public int Month { get; set; }
    }
}