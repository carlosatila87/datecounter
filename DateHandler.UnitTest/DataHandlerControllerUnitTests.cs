﻿using DateHandler.Controllers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static DateHandler.Controllers.DataHandlerController;

namespace DateHandler.UnitTest
{
    public class DataHandlerControllerUnitTests
    {
        [Fact]
        public void Test_BadRequest_CountWeekDays()
        {
            var controller = new DataHandlerController();

            var result = controller.CountWeekDays("20", "abc");

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Test_Valid_CountWeekDays()
        {
            var controller = new DataHandlerController();

            var result = controller.CountWeekDays("20190807", "20190810");

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Test_BadRequest_CountBusinessDays()
        {
            var controller = new DataHandlerController();

            var payload = new BusinessDayRequestV1()
            {
                firstDate = "20190807",
                secondDate = "20190808",
                holidays = new List<string>()
                {
                    "a"
                }
            };

            var result = controller.CountBusinessDays(payload);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Test_Valid_CountBusinessDays()
        {
            var controller = new DataHandlerController();

            var payload = new BusinessDayRequestV1()
            {
                firstDate = "20190807",
                secondDate = "20190808",
                holidays = new List<string>()
                {
                    "20190908",
                    "20190909"
                }
            };

            var result = controller.CountBusinessDays(payload);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Test_BadRequest_CountBusinessDaysV2()
        {
            var controller = new DataHandlerController();

            dynamic payload = new JObject();
            payload.firstDate = "20190807";
            payload.secondDate = "20190808";
            var holidays = new List<JObject>();
            dynamic holiday = new JObject();
            holiday.Date = "20190807";
            holiday.Category = 100;
            holiday.DayOfWeek = 0;
            holiday.Month = 10;
            holiday.Ordinality = 2;
            holidays.Add(holiday);
            payload.holidays = JToken.FromObject(holidays);

            var result = controller.CountBusinessDaysV2(payload);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Test_Valid_CountBusinessDaysV2()
        {
            var controller = new DataHandlerController();

            dynamic payload = new JObject();
            payload.firstDate = "20190807";
            payload.secondDate = "20190808";
            var holidays = new List<JObject>();
            dynamic holiday = new JObject();
            holiday.Date = "20190807";
            holiday.Category = 1;
            holiday.DayOfWeek = 0;
            holiday.Month = 10;
            holiday.Ordinality = 2;
            holidays.Add(holiday);
            payload.holidays = JToken.FromObject(holidays);

            var result = controller.CountBusinessDaysV2(payload);

            Assert.IsType<OkObjectResult>(result);
        }
    
    }
}
