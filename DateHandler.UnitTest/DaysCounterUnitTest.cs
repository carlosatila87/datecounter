using DateHandler.Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace DateHandler.UnitTest
{
    public class DaysCounterUnitTest
    {
        [Fact]
        public void Test_Invalid_Dates_On_WeekdaysBetweenTwoDates()
        {
            var daysCounter = new DaysCounter();
            Assert.Throws<ArgumentNullException>(() => daysCounter.WeekdaysBetweenTwoDates(DateTime.MinValue, DateTime.Today));
            Assert.Throws<ArgumentNullException>(() => daysCounter.WeekdaysBetweenTwoDates(DateTime.Today, DateTime.MinValue));
        }

        [Fact]
        public void Test_SecondDate_Before_FirstDate_On_WeekdaysBetweenTwoDates()
        {
            var daysCounter = new DaysCounter();
            Assert.Equal(0, daysCounter.WeekdaysBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(-1)));
        }

        [Fact]
        public void Test_Expected_Value_On_WeekdaysBetweenTwoDates()
        {
            var daysCounter = new DaysCounter();
            Assert.Equal(1, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019,8,6), new DateTime(2019,8,8)));
            Assert.Equal(3, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 10)));
            Assert.Equal(3, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11)));
            Assert.Equal(3, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 12)));
            Assert.Equal(4, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 13)));
            Assert.Equal(18, daysCounter.WeekdaysBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 31)));
        }

        [Fact]
        public void Test_Invalid_Dates_On_BusinessDayBetweenTwoDates()
        {
            var daysCounter = new DaysCounter();
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.MinValue, DateTime.Today, new List<DateTime>()));
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.MinValue, DateTime.Today, new List<PublicHoliday>()));
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.MinValue, new List<DateTime>()));
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.MinValue, new List<PublicHoliday>()));
        }

        [Fact]
        public void Test_Invalid_Date_With_SameDay_Category_On_BusinessDayBetweenTwoDates_With_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.SameDay
                }
            };
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(10), publicHolidays));
        }

        [Fact]
        public void Test_Invalid_Date_With_ExceptOnWeekend_Category_On_BusinessDayBetweenTwoDates_With_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ExceptOnWeekends
                }
            };
            Assert.Throws<ArgumentNullException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(10), publicHolidays));
        }

        [Fact]
        public void Test_Invalid_Month_With_ByOcurrence_Category_On_BusinessDayBetweenTwoDates_With_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ByOcurrence,
                    Month = 20
                }
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(10), publicHolidays));
        }

        [Fact]
        public void Test_SecondDate_Before_FirstDate_On_BusinessDayBetweenTwoDates()
        {
            var daysCounter = new DaysCounter();
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(-1), new List<DateTime>()));
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(DateTime.Today, DateTime.Today.AddDays(-1), new List<PublicHoliday>()));
        }

        [Fact]
        public void Test_Expected_Value_On_BusinessDayBetweenTwoDates_with_DateTimeList()
        {
            var daysCounter = new DaysCounter();
            Assert.Equal(1, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), new List<DateTime>()));
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), new List<DateTime>() { new DateTime(2019,8,7)}));
            //With Holiday on Weekend
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<DateTime>()));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<DateTime>() { new DateTime(2019, 8, 10) }));
            //With Holiday out of Date Range
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<DateTime>()));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<DateTime>() { new DateTime(2019, 8, 15) }));
            //With Duplicated Holiday
            Assert.Equal(2, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<DateTime>() { new DateTime(2019, 8, 9), new DateTime(2019, 8, 9) }));
        }

        [Fact]
        public void Test_Expected_Value_With_SameDay_Category_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.SameDay,
                    Date = new DateTime(2019,8,7)
                }
            };
            //Holiday as Week Day
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));
            
            //Holiday as Weekend Day
            publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.SameDay,
                    Date = new DateTime(2019,8,10)
                }
            };
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<PublicHoliday>()));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), publicHolidays));
        }

        [Fact]
        public void Test_Expected_Value_With_ExceptOnWeekends_Category_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ExceptOnWeekends,
                    Date = new DateTime(2019,8,7)
                }
            };
            //Holiday as Week Day
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));

            //Holiday as Weekend Day
            publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ExceptOnWeekends,
                    Date = new DateTime(2019,8,10)
                }
            };
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), new List<PublicHoliday>()));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 11), publicHolidays));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 12), publicHolidays));
            Assert.Equal(3, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 13), publicHolidays));
            Assert.Equal(4, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 14), publicHolidays));
        }

        [Fact]
        public void Test_Expected_Value_With_ByOcurrence_Category_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ByOcurrence,
                    Ordinality = HolidayOrdinality.First,
                    DayOfWeek = DayOfWeek.Wednesday,
                    Month = 8
                }
            };
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));
            publicHolidays.Add(new PublicHoliday()
            {
                Category = HolidayCategory.ByOcurrence,
                Ordinality = HolidayOrdinality.Second,
                DayOfWeek = DayOfWeek.Thursday,
                Month = 8
            });
            Assert.Equal(0, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 9), publicHolidays));
            publicHolidays.Add(new PublicHoliday()
            {
                Category = HolidayCategory.ByOcurrence,
                Ordinality = HolidayOrdinality.Third,
                DayOfWeek = DayOfWeek.Thursday,
                Month = 8
            });
            Assert.Equal(4, daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 16), publicHolidays));
        }

        [Fact]
        public void Test_Invalid_Category_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = (HolidayCategory)5,
                    Ordinality = HolidayOrdinality.First,
                    DayOfWeek = DayOfWeek.Wednesday,
                    Month = 8
                }
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));
        }

        [Fact]
        public void Test_Invalid_DayOfWeek_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ByOcurrence,
                    Ordinality = HolidayOrdinality.First,
                    DayOfWeek = (DayOfWeek)10,
                    Month = 8
                }
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));
        }

        [Fact]
        public void Test_Invalid_Ordinality_On_BusinessDayBetweenTwoDates_with_PublicHoliday_List()
        {
            var daysCounter = new DaysCounter();
            var publicHolidays = new List<PublicHoliday>()
            {
                new PublicHoliday()
                {
                    Category = HolidayCategory.ByOcurrence,
                    Ordinality = (HolidayOrdinality)20,
                    DayOfWeek = DayOfWeek.Sunday,
                    Month = 8
                }
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => daysCounter.BusinessDayBetweenTwoDates(new DateTime(2019, 8, 6), new DateTime(2019, 8, 8), publicHolidays));
        }
    }
}
